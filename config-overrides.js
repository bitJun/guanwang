const path = require('path');

module.exports = {
  webpack: function override(config) {
    config.resolve = {
      ...config.resolve,
      alias: { '~': path.resolve(__dirname, 'src') },
    };
    config.devServer = {
      historyApiFallback: true
    }
    return config;
  },
  devServer: function (configFunction) {
    return function (proxy, allowedHost) {
        const config = configFunction(proxy, allowedHost);
        config.disableHostCheck = true;
        config.historyApiFallback = true;
        return config;
    };
  },
  paths: function(paths, env) {
    // add customer config here

    return paths;
  }
};