import React from 'react';

import { Icon, IconProps } from '@chakra-ui/react';

export default function MealBox(props: IconProps) {
  return (
    <Icon viewBox="0 0 20 20" fill="transparent" {...props}>
      <rect
        x="0.781752"
        y="3.78175"
        width="18.3071"
        height="12.7875"
        rx="0.821842"
        stroke="#674E29"
        strokeWidth="1.5635"
      />
      <path d="M1 9H19" stroke="#674E29" strokeWidth="1.56" />
      <rect x="8" y="3" width="4" height="14" fill="#674E29" />
    </Icon>
  );
}
