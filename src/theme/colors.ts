import { Colors } from '@chakra-ui/react';

const colors: Colors = {
  primary: '#3B5940',
  primaryLight: '#DDEFE1',
  secondary: '#75917A',
  fontColor: '#333333',
  dark: '#333333',
  gray: {
    100: '#F8F8F8',
    200: '#F2F2F2',
    300: '#D9D9D9',
    400: '#C4C4C4',
    500: '#8D8D8D',
    700: '#454545',
  },
  danger: {
    200: '#FFF4F4',
    300: '#FCB4B4',
    600: '#E83B3B',
  },
};

export default colors;
