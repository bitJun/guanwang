import { ComponentSingleStyleConfig } from '@chakra-ui/react';

export enum InputVariant {
  OUTLINE = 'outline',
}

const Input: ComponentSingleStyleConfig = {
  baseStyle: {
    borderColor: 'gray.400',
    focusBorderColor: 'secondary',
    field: {
      color: 'fontColor',
    },
  },

  variants: {
    [InputVariant.OUTLINE]: {
      focusBorderColor: 'secondary',
      field: {
        borderColor: 'gray.400',
        _hover: {
          borderColor: 'gray.400',
        },
      },
    },
  },

  defaultProps: {
    variant: InputVariant.OUTLINE,
    focusBorderColor: 'secondary',
  },
};

export default Input;
