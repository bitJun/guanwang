import { useRoutes, RouteObject } from 'react-router-dom';

import Index from '~/pages/Home';
import Products from '~/pages/Products';
import AboutUs from '~/pages/AboutUs';
import APIpartner from '~/pages/APIpartner';
import Register from '~/pages/Register';
import TermsConditions from '~/pages/TermsConditions';

const routes: Array<RouteObject> = [
  {
    path: '',
    children: [
      {
        path: '/',
        element: <Index />,
      },
      {
        path: 'products',
        element: <Products />,
      },
      {
        path: 'aboutus',
        element: <AboutUs />,
      },
      {
        path: 'apipartner',
        element: <APIpartner />,
      },
      {
        path: 'register',
        element: <Register />,
      },
      {
        path: 'termsConditions',
        element: <TermsConditions />,
      },
    ]
  },
];

export default function Routes() {
  const element = useRoutes(routes);

  return element;
}
