import axios from 'axios';

type RegisterType = {
  type: string;
  companyName: string;
  contactName: string;
  contactPhone: string;
  api: string;
};

const AuthRegister = async ({
  type,
  companyName,
  contactName,
  contactPhone,
  api
}: RegisterType): Promise<{
  type: string;
  companyName: string;
  contactName: string;
  contactPhone: string;
  api: string;
} | null> => {
  try {
    const res = await axios.post<{
      type: string,
      companyName: string,
      contactName: string,
      contactPhone: string,
      api: string,
    }>(
      `${process.env.REACT_APP_AWS_HTTP_BASE_URL}/auth/register`, 
      {
        type,
        companyName,
        contactName,
        contactPhone,
        api,
      }
    );
    console.log('AuthRegister', res);
    return null;
  } catch (error) {
    return null;
  }
};
export default AuthRegister;