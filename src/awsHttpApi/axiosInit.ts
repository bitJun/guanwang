import axios from 'axios';

axios.interceptors.request.use(config => {
    if (!config.headers) config.headers = {}
    config.timeout = 20000;
    return config
});
axios.interceptors.response.use(data=> {
    return data;
});
  