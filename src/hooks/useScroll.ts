import { useEffect, useState } from 'react';
import throttle from 'lodash/throttle';

export type ScrollPositionInfo = {
  clientHeight: number;
  scrollHeight: number;
  scrollTop: number;
};

export default function useScroll(wait: number = 100) {
  const [positionInfo, setPositionInfo] = useState<ScrollPositionInfo>({
    clientHeight: document.body.clientHeight,
    scrollHeight: document.body.scrollHeight,
    scrollTop: document.body.scrollTop,
  });

  useEffect(() => {
    const handleScroll = throttle((e: Event) => {
      if (document.scrollingElement) {
        const { clientHeight, scrollHeight, scrollTop } = document.scrollingElement;

        setPositionInfo({
          clientHeight,
          scrollHeight,
          scrollTop,
        });
      }

      //console.log(e.target.scrollingElement);
    }, wait);

    document.addEventListener('scroll', handleScroll);

    return () => {
      document.removeEventListener('scroll', handleScroll);
    };
  }, [document, wait]);

  return { positionInfo };
}
