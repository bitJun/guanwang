export enum Week {
  SUN = 'SUN',
  MON = 'MON',
  TUE = 'TUE',
  WED = 'WED',
  THU = 'THU',
  FRI = 'FRI',
  SAT = 'SAT',
}

export const weekLabel = [
  {
    value: Week.SUN,
    label: '日',
  },
  {
    value: Week.MON,
    label: '一',
  },
  {
    value: Week.TUE,
    label: '二',
  },
  {
    value: Week.WED,
    label: '三',
  },
  {
    value: Week.THU,
    label: '四',
  },
  {
    value: Week.FRI,
    label: '五',
  },
  {
    value: Week.SAT,
    label: '六',
  },
];

export const monthLabel = [
  '一',
  '二',
  '三',
  '四',
  '五',
  '六',
  '七',
  '八',
  '九',
  '十',
  '十一',
  '十二',
];

export type Schedule = Partial<Record<Week, Array<{ open: number; close: number }>>>;
