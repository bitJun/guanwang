import { RecoilRoot } from 'recoil';
import { BrowserRouter } from 'react-router-dom';
import { ChakraProvider } from '@chakra-ui/react';
import theme from '~/theme';
import Routes from '~/routes';
import MediaQuery from './globalScript/MediaQuery';
import './awsHttpApi/axiosInit';
import '~/assets/css/assets.css';

function App() {
  return (
    <RecoilRoot>
      <ChakraProvider theme={theme}>
        <MediaQuery />
        <BrowserRouter>
          <Routes />
        </BrowserRouter>
      </ChakraProvider>
    </RecoilRoot>
  );
}

export default App;
