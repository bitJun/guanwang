import { atom } from 'recoil';

const modalControl = atom({
  key: 'globalModalControl',
  default: {
    isNavModalOpen: false,
    isRegisterTipModalOpen: false,
    isSuccessTipModalOpen: false,
  },
});

export default modalControl;
