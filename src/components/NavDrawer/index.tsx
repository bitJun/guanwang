import {
  Drawer,
  DrawerBody,
  DrawerContent,
  DrawerCloseButton,
  DrawerOverlay,
  DrawerHeader,
  HStack,
  VStack,
  Text,
  Icon,
} from '@chakra-ui/react';
import {
  IoRestaurantOutline,
  IoFileTrayOutline,
  IoReceiptOutline,
  IoPersonOutline,
  IoLogIn,
} from 'react-icons/io5';
import { useNavigate } from 'react-router-dom';
import { Logo } from '~/icons';

const navList = [
  {
    title: '首頁',
    icon: Logo,
    link: '',
  },
  {
    title: '全部餐點',
    icon: IoRestaurantOutline,
    link: 'filterMeal',
  },
  {
    title: '訂單',
    icon: IoReceiptOutline,
    link: 'orderList',
  },
  {
    title: '會員中心',
    icon: IoPersonOutline,
    link: 'memberCenter',
  },
  {
    title: '餐盒回收',
    icon: IoFileTrayOutline,
    link: 'mealBoxRecycling',
  },
];

type NavDrawerProps = {
  isOpen: boolean;

  onClose: () => void;
};

export default function NavDrawer({
  isOpen,

  onClose,
}: NavDrawerProps) {
  const navigate = useNavigate();

  const handleNavigate = (path: string) => {
    return navigate(`/${path}`);
  };

  return (
    <Drawer isOpen={isOpen} onClose={onClose} placement="left" size="xs">
      <DrawerOverlay />
      <DrawerContent maxW="250px">
        <DrawerHeader />
        <DrawerCloseButton
          bgColor="primary"
          color="white"
          borderRadius="full"
          _hover={{}}
          _focus={{}}
        />
        <DrawerBody>
          <VStack align="stretch" spacing={6}>
            {navList.map(({ title, icon, link }) => {
              return (
                <HStack
                  onClick={() => handleNavigate(link)}
                  key={title}
                  align="center"
                  cursor="pointer"
                >
                  <Icon as={icon} boxSize="1.5rem" color="dark" />
                  <Text fontSize="lg">{title}</Text>
                </HStack>
              );
            })}
            <HStack align="center" cursor="pointer">
              <Icon as={IoLogIn} boxSize="1.5rem" color="dark" />
              <Text fontSize="lg">登出</Text>
            </HStack>
          </VStack>
        </DrawerBody>
      </DrawerContent>
    </Drawer>
  );
}
