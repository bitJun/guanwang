import {
  Drawer,
  DrawerOverlay,
  DrawerContent,
  DrawerBody,
  DrawerCloseButton,
  Image,
  DrawerFooter,
  Text,
  HStack,
} from '@chakra-ui/react';
import {
  useNavigate,
  useLocation,
} from 'react-router-dom';
import { useSetRecoilState } from 'recoil';
import modalControlAtom from '~/recoil/atom/modalControl';
import logoPng from '~/assets/images/logo1.png';
import './index.css';

type LoginModalProps = {
  isOpen: boolean;
  onClose: () => void;
};

// TODO: error message 介面優化
export default function RegisterModal({ isOpen, onClose }: LoginModalProps) {
  const navigate = useNavigate();
  const location = useLocation();
  const setModalControl = useSetRecoilState(modalControlAtom);
  return (
    <Drawer isOpen={isOpen} onClose={onClose}>
      <DrawerOverlay />
      <DrawerContent
        w='277px'
        pt='22px'
        px='33px'
      >
        <DrawerCloseButton />
        <DrawerBody>
          <HStack
            justifyContent='center'
          >
            <Image
              src={logoPng}
              w='211px'
              h='40px'
              mb='24px'
            />
          </HStack>
          <Text
            className={`navbar_view_menulist_item ${location.pathname === '/' ? 'active' : null}`}
            onClick={(e)=>{setModalControl((prev) => ({ ...prev, isNavModalOpen: false }));navigate('/')}}
          >
            首页
          </Text>
          <Text
            className={`navbar_view_menulist_item ${location.pathname === '/products' ? 'active' : null}`}
            onClick={(e)=>{setModalControl((prev) => ({ ...prev, isNavModalOpen: false }));navigate('/products')}}
          >
            产品资源
          </Text>
          <Text
            className={`navbar_view_menulist_item ${location.pathname === '/apipartner' ? 'active' : null}`}
            onClick={(e)=>{setModalControl((prev) => ({ ...prev, isNavModalOpen: false }));navigate('/apipartner')}}
          >
            API/技术合作
          </Text>
          <Text
            className={`navbar_view_menulist_item ${location.pathname === '/aboutus' ? 'active' : null}`}
            onClick={(e)=>{setModalControl((prev) => ({ ...prev, isNavModalOpen: false }));navigate('/aboutus')}}
          >
            关于我们
          </Text>
        </DrawerBody>
        <DrawerFooter
          paddingInlineStart='0 !important'
          paddingInlineEnd='0 !important'
          justifyContent='center'
        >
          <Text
            className='navbar_view_register'
            onClick={(e)=>{setModalControl((prev) => ({ ...prev, isNavModalOpen: false }));navigate('/register')}}
          >
            免费注册
          </Text>
          <Text
            className='navbar_view_register'
          >
            登入
          </Text>
        </DrawerFooter>
      </DrawerContent>
    </Drawer>
  );
}
