import {
  HStack,
  Text,
  Box,
  Image,
  Flex
} from '@chakra-ui/react';
import {
  useNavigate,
  useLocation,
  Link
} from 'react-router-dom';
import { useRecoilValue, useSetRecoilState } from 'recoil';
import modalControlAtom from '~/recoil/atom/modalControl';
import mediaQueryStateAtom from '~/recoil/atom/mediaQueryState';
import './index.css';
import logoPng from '~/assets/images/logo1.png';
import morePng from '~/assets/images/more.png';

export type HeaderProps = {
  onOpen: () => void;
};

export default function Header({ onOpen }: HeaderProps) {
  const navigate = useNavigate();
  const location = useLocation();
  const { isLargerThan768 } = useRecoilValue(mediaQueryStateAtom);
  const setModalControl = useSetRecoilState(modalControlAtom);
  if (isLargerThan768) {
    return (
      <Box
        className='header'
      >
        <Box bg="white" className='header_main'>
          <HStack
            className='header_view'
          >
            <Image
              className='header_view_logo'
              src={logoPng}
            />
            <HStack
              className='header_view_menulist'
            >
              <Link
                className={`header_view_menulist_item ${location.pathname === '/' ? 'active' : null}`}
                to='/'
              >
                首页
              </Link>
              <Link
                className={`header_view_menulist_item ${location.pathname === '/products' ? 'active' : null}`}
                to='/products'
              >
                产品资源
              </Link>
              <Link
                className={`header_view_menulist_item ${location.pathname === '/apipartner' ? 'active' : null}`}
                to='/apipartner'
              >
                API/技术合作
              </Link>
              <Link
                className={`header_view_menulist_item ${location.pathname === '/aboutus' ? 'active' : null}`}
                to='/aboutus'
              >
                关于我们
              </Link>
            </HStack>
            <HStack
              alignItems='center'
              h='51px'
            >
              <Link
                className='header_view_register'
                to='/register'
              >
                免费注册
              </Link>
              <Text
                className='header_view_login'
                onClick={(e)=>window.location.href = 'http://b2b.tripintl.com/login'}
              >
                登入
              </Text>
            </HStack>
          </HStack>
        </Box>
      </Box>
    )
  } else {
    return (
      <Flex
        className='mobile_header'
      >
        <Image
          className='mobile_header_logo'
          src={logoPng}
        />
        <Image
          src={morePng}
          className='mobile_header_nav'
          onClick={() => setModalControl((prev) => ({ ...prev, isNavModalOpen: true }))}
        />
      </Flex>
    )
  }
}
