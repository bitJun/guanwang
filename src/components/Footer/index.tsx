import { useState } from 'react';
import {
  Box,
  Center,
  Text,
  Flex,
  HStack,
  Image
} from '@chakra-ui/react';
import {
  useLocation,
} from 'react-router-dom';
import ContentContainer from '~/containers/Content';
import { useRecoilValue } from 'recoil';
import mediaQueryStateAtom from '~/recoil/atom/mediaQueryState';
import './index.css';
import WechatImage from '~/assets/images/wechat.png';
import UpImage from '~/assets/images/up.png';
import DownImage from '~/assets/images/down.png';

//TODO:加上官網連結
export default function Footer() {
  const location = useLocation();
  const { isLargerThan768 } = useRecoilValue(mediaQueryStateAtom);
  const [showmenu1, setShowMenu1] = useState<Boolean>(false);
  const [showmenu2, setShowMenu2] = useState<Boolean>(false);
  const [showmenu3, setShowMenu3] = useState<Boolean>(false);
  const [showmenu4, setShowMenu4] = useState<Boolean>(false);
  if (isLargerThan768) {
    return (
      <ContentContainer
        className='footer_view'
      >
        {
          (location.pathname !== '/Register' && location.pathname !== '/TermsConditions') &&
          <Flex
            align="stretch"
            className='footer_view_main'
          >
            <HStack
              className='footer_view_main_section about'
            >
              <Text
                className='footer_view_main_section_title'
              >
                关于多会儿旅行
              </Text>
              <Text
                className='footer_view_main_section_desc'
              >
                公司介绍
              </Text>
              <Text
                className='footer_view_main_section_desc'
              >
                多会儿服务
              </Text>
            </HStack>
            <HStack
              className='footer_view_main_section product'
            >
              <Text
                className='footer_view_main_section_title'
              >
                多会儿产品
              </Text>
              <Text
                className='footer_view_main_section_desc'
              >
                全球酒店
              </Text>
              <Text
                className='footer_view_main_section_desc'
              >
                全球当地玩乐
              </Text>
              <Text
                className='footer_view_main_section_desc'
              >
                API分销
              </Text>
              <Text
                className='footer_view_main_section_desc'
              >
                技术合作
              </Text>
            </HStack>
            <HStack
              className='footer_view_main_section service'
            >
              <Text
                className='footer_view_main_section_title'
              >
                用户服务
              </Text>
              <Text
                className='footer_view_main_section_desc'
              >
                意见反馈
              </Text>
              <Text
                className='footer_view_main_section_desc'
              >
                用户协议
              </Text>
              <Text
                className='footer_view_main_section_desc'
              >
                联系我们
              </Text>
            </HStack>
            <HStack
              className='footer_view_main_section relation'
            >
              <Text
                className='footer_view_main_section_title'
              >
                关系伙伴
              </Text>
              <Text
                className='footer_view_main_section_desc'
              >
                旅点科技
              </Text>
              <Text
                className='footer_view_main_section_desc'
              >
                加入我们
              </Text>
            </HStack>
            <HStack
              className='footer_view_main_section business'
            >
              <Text
                className='footer_view_main_section_title'
              >
                业务咨询
              </Text>
              <Text
                className='footer_view_main_section_desc'
              >
                +86-13810248624
              </Text>
              <Text
                className='footer_view_main_section_desc'
              >
                tripintl@trirpintl.com
              </Text>
            </HStack>
            <HStack
              className='footer_view_main_section wechat'
            >
              <Text
                className='footer_view_main_section_title'
              >
                微信公众号
              </Text>
              <Image
                src={WechatImage}
                className='footer_view_main_section_img'
              />
            </HStack>
          </Flex>
        }
        <Center
          className='filing'
        >
          © 2017-2022 tripintl.com 沪ICP备18018917号 上海临行网络科技有限公司<br/>
          北京市朝阳区光华路9号世贸天阶大厦24层
        </Center>
      </ContentContainer>
    )
  } else {
    return (
      <ContentContainer
        className='mobile_footer'
      >
        {
          (location.pathname !== '/Register' && location.pathname !== '/TermsConditions') &&
          <Box>
            <Box
              className='mobile_footer_menu'
            >
              <Flex
                className='mobile_footer_menu_main'
                onClick={(e:any)=>{
                  setShowMenu1(!showmenu1);
                }}
              >
                <Text
                  className='mobile_footer_menu_title'
                >
                  关于多会儿旅行
                </Text>
                <Image
                  src={showmenu1 ? DownImage : UpImage}
                  className='mobile_footer_menu_title_icon'
                />
              </Flex>
              {
                showmenu1 &&
                <Box
                  className='mobile_footer_submenu'
                >
                  <Text
                    className='mobile_footer_submenu_title'
                  >
                    公司介绍
                  </Text>
                  <Text
                    className='mobile_footer_submenu_title'
                  >
                    多会儿服务
                  </Text>
                </Box>
              }
            </Box>
            <Box
              className='mobile_footer_menu'
            >
              <Flex
                className='mobile_footer_menu_main'
                onClick={(e:any)=>{
                  setShowMenu2(!showmenu2);
                }}
              >
                <Text
                  className='mobile_footer_menu_title'
                >
                  多会儿产品
                </Text>
                <Image
                  src={showmenu2 ? DownImage : UpImage}
                  className='mobile_footer_menu_title_icon'
                />
              </Flex>
              {
                showmenu2 &&
                <Box
                  className='mobile_footer_submenu'
                >
                  <Text
                    className='mobile_footer_submenu_title'
                  >
                    全球酒店
                  </Text>
                  <Text
                    className='mobile_footer_submenu_title'
                  >
                    全球当地玩乐
                  </Text>
                  <Text
                    className='mobile_footer_submenu_title'
                  >
                    API分销
                  </Text>
                  <Text
                    className='mobile_footer_submenu_title'
                  >
                    技术合作
                  </Text>
                </Box>
              }
            </Box>
            <Box
              className='mobile_footer_menu'
            >
              <Flex
                className='mobile_footer_menu_main'
                onClick={(e:any)=>{
                  setShowMenu3(!showmenu3);
                }}
              >
                <Text
                  className='mobile_footer_menu_title'
                >
                  用户服务
                </Text>
                <Image
                  src={showmenu3 ? DownImage : UpImage}
                  className='mobile_footer_menu_title_icon'
                />
              </Flex>
              {
                showmenu3 &&
                <Box
                  className='mobile_footer_submenu'
                >
                  <Text
                    className='mobile_footer_submenu_title'
                  >
                    意见反馈
                  </Text>
                  <Text
                    className='mobile_footer_submenu_title'
                  >
                    用户协议
                  </Text>
                  <Text
                    className='mobile_footer_submenu_title'
                  >
                    联系我们
                  </Text>
                </Box>
              }
            </Box>
            <Box
              className='mobile_footer_menu'
            >
              <Flex
                className='mobile_footer_menu_main'
                onClick={(e:any)=>{
                  setShowMenu4(!showmenu4);
                }}
              >
                <Text
                  className='mobile_footer_menu_title'
                >
                  关系伙伴
                </Text>
                <Image
                  src={showmenu4 ? DownImage : UpImage}
                  className='mobile_footer_menu_title_icon'
                />
              </Flex>
              {
                showmenu4 &&
                <Box
                  className='mobile_footer_submenu'
                >
                  <Text
                    className='mobile_footer_submenu_title'
                  >
                    旅点科技
                  </Text>
                  <Text
                    className='mobile_footer_submenu_title'
                  >
                    加入我们
                  </Text>
                </Box>
              }
            </Box>
            <Box
              className='mobile_footer_menu'
            >
              <Flex
                className='mobile_footer_menu_main'
              >
                <Text
                  className='mobile_footer_menu_title'
                >
                  业务咨询
                </Text>
              </Flex>
              <Box
                className='mobile_footer_submenu'
              >
                <Text
                  className='mobile_footer_submenu_title'
                >
                  +86-13810248624
                </Text>
                <Text
                  className='mobile_footer_submenu_title'
                >
                  tripintl@trirpintl.com
                </Text>
              </Box>
            </Box>
          </Box>
        }
        <Center
          className='mobile_filing'
        >
          © 2017-2022 tripintl.com 沪ICP备18018917号<br/>上海临行网络科技有限公司<br/>
          北京市朝阳区光华路9号世贸天阶大厦24层
        </Center>
      </ContentContainer>
    )
  }
}
