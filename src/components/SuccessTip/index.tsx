import {
    Button,
    Center,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalBody,
    Text,
} from '@chakra-ui/react';

type WechatPayModalProps = {
  isOpen: boolean;
  onClose: () => void;
};
// TODO: error message 介面優化
export default function SuccessTip({ isOpen, onClose }: WechatPayModalProps) {
    return (
        <Modal
            isOpen={isOpen}
            onClose={onClose}
            size="sm"
            isCentered
            scrollBehavior="inside"
        >
            <ModalOverlay />
            <ModalContent
                w='318px'
                boxShadow='0px 2.82028px 11.2811px rgba(131, 130, 251, 0.55)'
                borderRadius='14px'
            >
                <ModalBody
                    pt='25px'
                    pb='20px'
                >
                    <Center>
                        <Text
                            h='42px'
                            fontFamily='Roboto'
                            fontWeight='700'
                            fontSize='28px'
                            color='#002C5E'
                            mb='10px'
                        >
                            提交成功
                        </Text>
                    </Center>
                    <Center>
                        <Text
                            w='187px'
                            fontFamily='PingFang SC'
                            fontSize='14px'
                            color='#364F6B'
                            textAlign='center'
                        >
                            感谢您的注册，我们将尽快为您开通账号 2 - 3<br/> 天内将由专人与您联系！
                        </Text>
                    </Center>
                    <Center
                        mt='16px'
                    >
                        <Button
                            w='80px'
                            h='48px'
                            color='#ffffff'
                            lineHeight='48px'
                            bgColor='#2D6CDF'   
                            textAlign='center'
                            borderWidth='1px'
                            borderColor='#2D6CDF'
                            fontSize='14px'
                            borderRadius='20px'
                            onClick={onClose}
                        >
                            完成
                        </Button>
                    </Center>
                </ModalBody>
            </ModalContent>
        </Modal>
    )
}
