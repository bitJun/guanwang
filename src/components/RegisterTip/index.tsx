import {
    Button,
    Center,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalBody,
    Text,
} from '@chakra-ui/react';

type WechatPayModalProps = {
  isOpen: boolean;
  onClose: () => void;
};
// TODO: error message 介面優化
export default function RegisterTip({ isOpen, onClose }: WechatPayModalProps) {
    const Login = (event:any) => {
        window.open('http://b2b.tripintl.com/login', '_blank');
    }
    return (
        <Modal
            isOpen={isOpen}
            onClose={onClose}
            size="sm"
            isCentered
            scrollBehavior="inside"
        >
            <ModalOverlay />
            <ModalContent
                w='318px'
                boxShadow='0px 2.82028px 11.2811px rgba(131, 130, 251, 0.55)'
                borderRadius='14px'
            >
                <ModalBody
                    pt='25px'
                    pb='20px'
                >
                    <Center>
                        <Text
                            h='42px'
                            fontFamily='Roboto'
                            fontWeight='700'
                            fontSize='28px'
                            color='#002C5E'
                            mb='10px'
                        >
                            已注册
                        </Text>
                    </Center>
                    <Center>
                        <Text
                            w='187px'
                            fontFamily='PingFang SC'
                            fontSize='14px'
                            color='#364F6B'
                            textAlign='center'
                        >
                            此手机已注册过，请直接登入<br/> 若有疑问，可联系客服专线<br/> +86 13810248624 
                        </Text>
                    </Center>
                    <Center
                        mt='16px'
                    >
                        <Button
                            w='80px'
                            h='48px'
                            color='#2D6CDF'
                            lineHeight='48px'
                            bgColor='white'   
                            textAlign='center'
                            borderWidth='1px'
                            borderColor='#2D6CDF'
                            fontSize='14px'
                            borderRadius='20px'
                            mr='9px'
                            onClick={onClose}
                        >
                            返回页面
                        </Button>
                        <Button
                            w='80px'
                            h='48px'
                            color='#ffffff'
                            lineHeight='48px'
                            bgColor='#2D6CDF'   
                            textAlign='center'
                            borderWidth='1px'
                            borderColor='#2D6CDF'
                            fontSize='14px'
                            borderRadius='20px'
                            onClick={(e)=>Login(e)}
                        >
                            直接登入
                        </Button>
                    </Center>
                </ModalBody>
            </ModalContent>
        </Modal>
    )
}
