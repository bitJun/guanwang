import {
    HStack,
    Box,
    Image,
    Text,
    Button,
} from '@chakra-ui/react';
import {
  useNavigate,
} from 'react-router-dom';
import { useRecoilValue } from 'recoil';
import mediaQueryStateAtom from '~/recoil/atom/mediaQueryState';
import PageContainer from '~/containers/PageContainer';
import ContentContainer from '~/containers/Content';
import FrameIcon from '~/assets/images/Frame.png';
import Frame from '~/assets/images/about/Frame.png';
import Line from '~/assets/images/product/Line.png';
import SmallLine from '~/assets/images/smallLine.png';
import Icon1 from '~/assets/images/product/icon1.png';
import Icon2 from '~/assets/images/product/icon2.png';
import Icon3 from '~/assets/images/product/icon3.png';
import Icons from '~/assets/images/product/icons.png';
import Pic from '~/assets/images/product/pic.png';
import Pic1 from '~/assets/images/about/pic.png';
import Pic2 from '~/assets/images/product/pic2.png';
import Pic3 from '~/assets/images/product/pic3.png';
import Pic4 from '~/assets/images/product/pic4.png';
import titleIcon from '~/assets/images/product/title.png';
import titleIcon1 from '~/assets/images/product/title1.png';
import mobileIcon1 from '~/assets/images/product/mobile_icon1.jpeg';
import Group from '~/assets/images/product/Group.png';
import Vector from '~/assets/images/about/Vector.png';
import './index.css';

const partnerlist:any = [
    {
        id: 0,
        title: '灵活支付',
        desc: '支持支付宝，微信，国内外信用卡，以及额度支付'
    },
    {
        id: 1,
        title: '聚焦资源',
        desc: '全球酒店与当地玩乐，源头供应保障'
    },
    {
        id: 2,
        title: '专业客服团队',
        desc: '专业客服将7*24小时为客户保驾护航'
    },
    {
        id: 3,
        title: '价格保证',
        desc: '真正的底价共享，成本更低，支持比价'
    },
    {
        id: 4,
        title: '跨平台预定',
        desc: '支持电脑，移动端，微信小程序等跨平台操作'
    },
    {
        id: 5,
        title: '同业平台',
        desc: '适合同业操作习惯，只给最理想的方案'
    },
    {
        id: 6,
        title: '多面技术支撑',
        desc: '支持同业在线采购，亦可用API直连资源'
    },
    {
        id: 7,
        title: '组合更优惠',
        desc: '自由行本是多个产品分别预定，组合买更优惠'
    }
]
export default function Home () {
    const { isLargerThan768 } = useRecoilValue(mediaQueryStateAtom);
    const navigate = useNavigate();
    const register = (event: any) => {
        navigate('/register');
    }
    return (
        <PageContainer>
            <Box
                className='product_view'
            >
                <Box
                    className='product_view_container'
                >
                    {
                        isLargerThan768 ? (
                            <Box
                                className='product_view_banner'
                            >
                                <Text
                                    className='product_view_banner_title'
                                >
                                    自由行产品采购从未如此简单
                                </Text>
                                <Text
                                    className='product_view_banner_desc'
                                >
                                    与旅业相融共生的一站式自由行采购平台
                                </Text>
                                <Button
                                    className='product_view_banner_action'
                                    onClick={(e)=>{register(e)}}
                                >
                                    免费注册
                                </Button>
                                <Image
                                    src={Frame}
                                    className='product_view_banner_frame'
                                />
                                <Image
                                    src={Vector}
                                    className='product_view_banner_vector'
                                />
                                <Image
                                    src={Pic1}
                                    className='product_view_banner_pic'
                                />
                            </Box>
                        ) : (
                            <Box
                                className='product_view_banner'
                            >
                                <Image
                                    src={mobileIcon1}
                                    className='product_view_banner_icon'
                                />
                            </Box>
                        )
                    }
                    <ContentContainer
                        bg='#2D6CDF'
                        h={isLargerThan768 ? '269px' : '299px'}
                        mb={isLargerThan768 ? '0' : '20px'}
                    >
                        <Box
                            className='product_view_desc'
                        >
                            <Text
                                className='product_view_desc_title'
                            >
                                自由行需求得心应手
                            </Text>
                            <Image
                                src={isLargerThan768 ? Line : SmallLine}
                                className='product_view_desc_line'
                            />
                            <Text
                                className='product_view_desc_value'
                            >
                                自由行是同业面对旅客繁多冗杂的需求，匹配海量的全球旅游资源，着实不易。{isLargerThan768 && <br/>}
                                多会儿旅行让问题变得易如反掌，实现了自由行产品一站式采购，同业预定不费吹灰之力即可获得一套完整的自由行产品组合方案。{isLargerThan768 && <br/>}
                                无论是酒店还是当地玩乐， 不用多个平台，不用多次交易，通过多会儿一站搞定！  
                            </Text>
                        </Box>
                    </ContentContainer>
                    <Box
                        className='product_view_characteristic'
                    >
                        <HStack
                            className='product_view_characteristic_list'
                        >
                            <HStack
                                className='product_view_characteristic_list_item'
                            >
                                <Image
                                    src={Icon1}
                                    className='product_view_characteristic_list_item_icon'
                                />
                                <Box
                                    marginInlineStart='0 !important'
                                >
                                    <Text
                                        className='product_view_characteristic_list_item_title'
                                    >
                                        酒跨平台
                                        预定系统
                                    </Text>
                                    <Text
                                        className='product_view_characteristic_list_item_more'
                                    >
                                        电脑网页， 移动端，{isLargerThan768 && <br/>}
                                        小程序
                                    </Text>
                                </Box>
                            </HStack>
                            <HStack
                                className='product_view_characteristic_list_item'
                            >
                                <Image
                                    src={Icon3}
                                    className='product_view_characteristic_list_item_icon'
                                />
                                <Box
                                    marginInlineStart='0 !important'
                                >
                                    <Text
                                        className='product_view_characteristic_list_item_title'
                                    >
                                        旅游同业
                                        结算价格
                                    </Text>
                                    <Text
                                        className='product_view_characteristic_list_item_more'
                                    >
                                        同业价格优势{isLargerThan768 && <br/>}
                                        多结算模式选择
                                    </Text>
                                </Box>
                            </HStack>
                            <HStack
                                className='product_view_characteristic_list_item'
                            >
                                <Image
                                    src={Icon2}
                                    className='product_view_characteristic_list_item_icon'
                                />
                                <Box
                                    marginInlineStart='0 !important'
                                >
                                    <Text
                                        className='product_view_characteristic_list_item_title'
                                    >
                                        100万+
                                        产品选择
                                    </Text>
                                    <Text
                                        className='product_view_characteristic_list_item_more'
                                    >
                                        涵盖多产品类型{isLargerThan768 && <br/>}
                                        一站搞定
                                    </Text>
                                </Box>
                            </HStack>
                        </HStack>
                    </Box>
                    <HStack
                        className='product_view_section'
                    >
                        <Image
                            src={Pic}
                            className='product_view_section_icon'
                        />
                        <Box
                            className='product_view_section_content'
                        >
                            <HStack
                                className='product_view_section_content_title'
                            >
                                <Text
                                    className='product_view_section_content_title_value'
                                >
                                    全球酒店预定
                                </Text>
                                <Image
                                    src={FrameIcon}
                                    className='product_view_section_content_title_icon'
                                />
                            </HStack>
                            <Text
                                className='product_view_section_content_subtitle'
                            >
                                超120万全球酒店资源
                            </Text>
                            <Text
                                className='product_view_section_content_desc'
                            >
                                酒店覆盖6大洲，超200个国家地区，实时预订库存超60万，价格优惠，预定无忧！
                            </Text>
                            <Text
                                className='product_view_section_content_subtitle'
                            >
                                随时随地，实时预定
                            </Text>
                            <Text
                                className='product_view_section_content_desc'
                            >
                                多会儿旅行的酒店资源99%为立即确认房型，及时得知预定结果。 加上多会儿的移动端平台，同业随时随地都可以实时预定！
                            </Text>
                            <Text
                                className='product_view_section_content_subtitle'
                            >
                                订房保障
                            </Text>
                            <Text
                                className='product_view_section_content_desc'
                            >
                                多会儿旅行提供全方位的预定保障。 订单确认后如出现问题，多会儿旅行会主动协调， 以最高效的方式解决问题，原单升级，承担差价。
                            </Text>
                            <Button
                                className='product_view_section_content_action'
                                onClick={(e)=>{register(e)}}
                            >
                                立即注册
                            </Button>
                        </Box>
                    </HStack>
                    <HStack
                        className='product_view_section'
                    >
                        <Box
                            className='product_view_section_contents'
                        >

                            {
                                !isLargerThan768 &&
                                <Image
                                    src={Pic2}
                                    className='product_view_section_icons'
                                />
                            }
                            <HStack
                                className='product_view_section_content_title'
                            >
                                <Text
                                    className='product_view_section_content_title_value'
                                >
                                    全球当地玩乐预定
                                </Text>
                                <Image
                                    src={FrameIcon}
                                    className='product_view_section_content_title_icon'
                                />
                            </HStack>
                            <Text
                                className='product_view_section_content_subtitle'
                            >
                                各类型自由行产品，应有尽有
                            </Text>
                            <Text
                                className='product_view_section_content_desc'
                            >
                                资源覆盖全球600个城市地区，产品包括当季热门活动、景区、交通等，规划完美行程，出游无负担！
                            </Text>
                            <Image
                                src={isLargerThan768 ? Icons : Pic4}
                                className='product_view_section_content_img'
                            />
                            <Button
                                className='product_view_section_content_action'
                                onClick={(e)=>{register(e)}}
                            >
                                立即注册
                            </Button>
                        </Box>
                        {
                            isLargerThan768 &&
                            <Image
                                src={Pic2}
                                className='product_view_section_icons'
                            />
                        }
                    </HStack>
                    <Box
                        className='product_view_aprtner'
                    >
                        <HStack
                            className='product_view_aprtner_title'
                        >
                            <Image  
                                src={isLargerThan768 ? titleIcon1 : titleIcon}
                                className='product_view_aprtner_title_value'
                            />
                        </HStack>
                        <HStack
                            className='product_view_aprtner_list'
                        >
                            {
                                partnerlist.map((item:any)=>
                                    <HStack
                                        className='product_view_aprtner_list_item'
                                        key={item.id}
                                    >
                                        <Image
                                            src={Group}
                                            className='product_view_aprtner_list_item_icon'
                                        />
                                        <Text
                                            className='product_view_aprtner_list_item_title'
                                        >
                                            {item.title}
                                        </Text>
                                        <Text
                                            className='product_view_aprtner_list_item_desc'
                                        >
                                            {item.desc}
                                        </Text>
                                    </HStack>    
                                )
                            }
                        </HStack>
                    </Box>
                    <ContentContainer
                        bg='#2D6CDF'
                        h={isLargerThan768 ? '420px' : '565px'}
                    >
                        <Box
                            className='product_view_tip'
                        >
                            <HStack
                                className='product_view_tip_main'
                            >
                                <Image
                                    src={Pic3}
                                    className='product_view_tip_main_img'
                                />
                                <Box
                                    className='product_view_tip_main_content'
                                >
                                    <Text
                                        className='product_view_tip_main_content_title'
                                    >
                                        现在注册<br/>获得新客优惠大礼包
                                    </Text>
                                    <Image
                                        src={isLargerThan768 ? Line : SmallLine}
                                        className='product_view_tip_main_content_line'
                                    />
                                    <Text
                                        className='product_view_tip_main_content_desc'
                                    >
                                        不玩虚的，实打实的优惠
                                    </Text>
                                    <Button
                                        className='product_view_tip_main_content_action'
                                        onClick={(e)=>{register(e)}}
                                    >
                                        免费注册
                                    </Button>
                                </Box>
                            </HStack>
                        </Box>
                    </ContentContainer>
                </Box>
            </Box>
        </PageContainer>
    )
}