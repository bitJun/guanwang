import {
    HStack,
    Box,
    Image,
    Text,
    Button
} from '@chakra-ui/react';
import {
  useNavigate,
} from 'react-router-dom';
import { useRecoilValue } from 'recoil';
import mediaQueryStateAtom from '~/recoil/atom/mediaQueryState';
import PageContainer from '~/containers/PageContainer';
import ContentContainer from '~/containers/Content';
import Line from '~/assets/images/product/Line.png';
import SmallLine from '~/assets/images/smallLine.png';
import location from '~/assets/images/API/location.png';
import Pic from '~/assets/images/API/pic1.png';
import Pic1 from '~/assets/images/API/pic.png';
import Pic2 from '~/assets/images/API/pic2.png';
import Pic3 from '~/assets/images/API/pic3.png';
import BN from '~/assets/images/API/BN.png';
import title from '~/assets/images/API/title.png';
import bannerImg from '~/assets/images/API/bannerImg.jpeg';
import FrameIcon from '~/assets/images/Frame.png';
import advantage from '~/assets/images/API/advantage.png';
import advantage_mobile from '~/assets/images/API/advantage_mobile.png';
import './index.css';

export default function Home () {
    const { isLargerThan768 } = useRecoilValue(mediaQueryStateAtom);
    const navigate = useNavigate();
    const register = (event: any) => {
        navigate('/register');
    }
    return (
        <PageContainer>
            <Box
                className='partner_view'
            >
                {
                    isLargerThan768 ? (
                        <Box
                            className='partner_view_banner'
                        >
                            <Image
                                src={BN}
                                className='partner_view_banner_bn'
                            />
                            <Text
                                className='partner_view_banner_title'
                            >
                                行业级别的分销API
                            </Text>
                            <Text
                                className='partner_view_banner_desc'
                            >
                                API对接从未如此简单
                            </Text>
                            <Button
                                className='partner_view_banner_action'
                            >
                                申请对接
                            </Button>
                        </Box>
                    ) : (
                        <Box
                            className='partner_view_banner'
                        >
                            <Image
                                src={bannerImg}
                                className='partner_view_banner_bn'
                            />
                        </Box>
                    )
                }
                <HStack
                    className='partner_view_list'
                >
                    <Box
                        className='partner_view_list_item'
                    >
                        <HStack
                            className='partner_view_list_title'
                        >
                            <Image
                                src={location}
                                className='partner_view_list_title_icon'
                            />
                            <Text
                                className='partner_view_list_title_value'
                            >
                                开发体验佳
                            </Text>
                        </HStack>
                        <Text
                            className='partner_view_list_value'
                        >
                            拥有完整的文档以及成熟的接口，可以立即在沙箱环境进行开发及测试。
                        </Text>
                    </Box>
                    <Box
                        className='partner_view_list_item'
                    >
                        <HStack
                            className='partner_view_list_title'
                        >
                            <Image
                                src={location}
                                className='partner_view_list_title_icon'
                            />
                            <Text
                                className='partner_view_list_title_value'
                            >
                                持续迭代
                            </Text>
                        </HStack>
                        <Text
                            className='partner_view_list_value'
                        >
                            我们的开发团队会持续根据現況技术需求的变化调整接口，提供最佳的实践方案给开发者。
                        </Text>
                    </Box>
                    <Box
                        className='partner_view_list_item'
                    >
                        <HStack
                            className='partner_view_list_title'
                        >
                            <Image
                                src={location}
                                className='partner_view_list_title_icon'
                            />
                            <Text
                                className='partner_view_list_title_value'
                            >
                                完整接口功能
                            </Text>
                        </HStack>
                        <Text
                            className='partner_view_list_value'
                        >
                            接口中可获得所有需要的数据以及功能，包含静态数据，实时价格，订单管理以及价格变化通知。
                        </Text>
                    </Box>
                </HStack>
                <Box
                    className='partner_view_advantage'
                >
                    <HStack
                        className='partner_view_advantage_title'
                    >
                        <Image
                            src={title}
                            className='partner_view_advantage_title_icon'
                        />
                    </HStack>
                    <HStack
                        className='partner_view_advantage_section'
                    >
                        <Image
                            src={Pic1}
                            className='partner_view_advantage_section_icon'
                        />
                        <Box
                            className='partner_view_advantage_section_content'
                        >
                            <HStack
                                className='partner_view_advantage_section_content_title'
                            >
                                <Text
                                    className='partner_view_advantage_section_content_title_value'
                                >
                                    多元化合作
                                </Text>
                                {
                                    !isLargerThan768 &&
                                    <Image
                                        src={FrameIcon}
                                        className='partner_view_advantage_section_content_title_icon'
                                    />
                                }
                            </HStack>
                            <Text
                                className='partner_view_advantage_section_content_desc'
                            >
                                除了传统的全套嵌入模式，多会儿分销API的设计架构最大程度适配多元化的合作可能性， 不论是纯产品数据模式到佣金分润模式，多会儿分销API均可无缝隙适配。
                            </Text>
                        </Box>
                    </HStack>
                    <HStack
                        className='partner_view_advantage_section'
                    >
                        {
                            !isLargerThan768 &&
                            <Image
                                src={Pic2}
                                className='partner_view_advantage_section_icon'
                            />
                        }
                        <Box
                            className='partner_view_advantage_section_contents'
                        >
                            <HStack
                                className='partner_view_advantage_section_contents_title'
                            >
                                <Text
                                    className='partner_view_advantage_section_contents_title_value'
                                >
                                    开发效率快
                                </Text>
                                <Image
                                    src={FrameIcon}
                                    className='partner_view_advantage_section_contents_title_icon'
                                />
                            </HStack>
                            <Text
                                className='partner_view_advantage_section_contents_desc'
                            >
                                通过完善的开发体验，多会儿分销API<br/>帮助开发者更快速的完成对接。
                            </Text>
                            <Image
                                src={isLargerThan768 ? advantage : advantage_mobile}
                                className='partner_view_advantage_section_contents_img'
                            />
                        </Box>
                        {
                            isLargerThan768 &&
                            <Image
                                src={Pic2}
                                className='partner_view_advantage_section_icon'
                            />
                        }
                    </HStack>
                    <HStack
                        className='partner_view_advantage_section'
                    >
                        <Image
                            src={Pic3}
                            className='partner_view_advantage_section_icon'
                        />
                        <Box
                            className='partner_view_advantage_section_content'
                        >
                            <HStack
                                className='partner_view_advantage_section_content_title'
                            >
                                <Text
                                    className='partner_view_advantage_section_content_title_value'
                                >
                                    稳定的技术架构
                                </Text>
                                {
                                    !isLargerThan768 &&
                                    <Image
                                        src={FrameIcon}
                                        className='partner_view_advantage_section_content_title_icon'
                                    />
                                }
                                </HStack>
                            <Text
                                className='partner_view_advantage_section_content_desc'
                            >
                                基于微服务架构以及多年的性能优化，多会儿分销API架构极为稳定。 从接口响应时间到高峰期的并发载重量体，都能让客户拥有稳定又省心的对体验。
                            </Text>
                        </Box>
                    </HStack>
                </Box>
                <ContentContainer
                    bg='#2D6CDF'
                    h={isLargerThan768 ? '420px' : '565px'}
                    className='section'
                >
                    <HStack
                        className='partner_view_develop'
                    >
                        <Image
                            src={Pic}
                            className='partner_view_develop_img'
                        />
                        <Box
                            className='partner_view_develop_content'
                        >
                            <Text
                                className='partner_view_develop_content_title'
                            >
                                多会儿技术开发服务
                            </Text>
                            <Image
                                src={isLargerThan768 ? Line : SmallLine}
                                className='partner_view_develop_content_line'
                            />
                            <Text
                                className='partner_view_develop_content_desc'
                            >
                                不论是API的对接，网页开发或量身定做的需求，提供完整的开发服务，{isLargerThan768 && <br/>}让您无需投入开发人力资源，即可获得希望的技术产品。
                            </Text>
                            <Button
                                className='partner_view_develop_content_action'
                                onClick={(e)=>register(e)}
                            >
                                免费注册   
                            </Button>
                        </Box>
                    </HStack>
                </ContentContainer>   
            </Box>
        </PageContainer>
    )
}