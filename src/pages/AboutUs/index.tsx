import { useState } from 'react';
import {
    VStack,
    HStack,
    Box,
    Text,
    Image,
} from '@chakra-ui/react';
import { useRecoilValue } from 'recoil';
import mediaQueryStateAtom from '~/recoil/atom/mediaQueryState';
import PageContainer from '~/containers/PageContainer';
import FrameIcon from '~/assets/images/Frame.png';
import Frame from '~/assets/images/about/Frame.png';
import FrameImg from '~/assets/images/about/FrameIcon.png';
import Pic from '~/assets/images/about/pic.png';
import Pic1 from '~/assets/images/about/pic1.png';
import Pic2 from '~/assets/images/about/pic2.png';
import Group from '~/assets/images/about/Group.png';
import Vector from '~/assets/images/about/Vector.png';
import Banner from '~/assets/images/about/banner.jpeg';
import Icon from '~/assets/images/about/icon.jpeg';
import Title from '~/assets/images/about/title.png';
import UpImage from '~/assets/images/up.png';
import DownImage from '~/assets/images/down.png';
import './index.css';

export default function Home () {
    const { isLargerThan768 } = useRecoilValue(mediaQueryStateAtom);
    const [showmenu1, setShowMenu1] = useState<Boolean>(false);
    const [showmenu2, setShowMenu2] = useState<Boolean>(false);
    const [showmenu3, setShowMenu3] = useState<Boolean>(false);
    const [showmenu4, setShowMenu4] = useState<Boolean>(false);
    const [showmenu5, setShowMenu5] = useState<Boolean>(false);
    return (
        <PageContainer>
            <Box
                className='about_view'
            >
                <Box
                    className='about_view_container'
                >
                    {
                        isLargerThan768 ? (
                            <Box
                                marginInlineStart='0 !important'
                            >
                                <Image
                                    src={Vector}
                                    className='about_view_container_Vector'
                                />
                                <Image
                                    src={Frame}
                                    className='about_view_container_Frame'
                                />
                                <Image
                                    src={FrameImg}
                                    className='about_view_container_FrameImg'
                                />
                                <Text
                                    className='about_view_title'
                                    as='h1'
                                >
                                    关于我们
                                </Text>
                                <Text
                                    className='about_view_subtitle'
                                >
                                    多会儿旅行致力成为旅游同业的一站式采购平台
                                </Text>
                            </Box>
                        ) : (
                            <Box
                                h='473px'
                                marginInlineStart='0 !important'
                            >
                                <Image
                                    src={Banner}
                                    className='about_view_banner'
                                />  
                            </Box>
                        )
                    }
                    <HStack
                        className='about_view_container_scetion'
                    >
                        <Image
                            src={isLargerThan768 ? Pic : Icon}
                            className='about_view_container_scetion_img'
                        />
                        <Box
                            className='about_view_container_scetion_content'
                        >
                            <Image
                                className='about_view_container_scetion_content_title'
                                src={Title}
                            />
                            <Text
                                className='about_view_container_scetion_content_subtitle'
                            >
                                多会儿旅行致力成为{!isLargerThan768 && <br/>}旅游同业的一站式采购平台。 
                            </Text>
                            <Text
                                className='about_view_container_scetion_content_desc'
                            >
                                旅点科技旗下的旅游品牌，【多会儿旅行】起始于后疫情时代，聚焦自由行市场的趋势。
                            </Text>
                            <Text
                                className='about_view_container_scetion_content_desc'
                            >
                                成立的初衷是为赋能旅游同业拥有全面的产品资源，帮助旅游同业扩大客户群体，提供更高效的服务。 
                            </Text>
                        </Box>
                    </HStack>
                    <Box
                        className='about_view_container_scetion2'
                    >
                        {
                            isLargerThan768 &&
                            <HStack
                                className='about_view_container_scetion2_title'
                            >
                                <Text
                                    className='about_view_container_scetion2_title_value'
                                >
                                    多会儿价值观
                                </Text>
                                <Image
                                    src={FrameIcon}
                                    className='about_view_container_scetion2_title_icon'
                                />
                            </HStack>
                        }
                        <HStack
                            className='about_view_container_scetion2_list'
                        >
                            <Box
                                className='about_view_container_scetion2_list_item'
                            >
                                <Image
                                    src={Group}
                                    className='about_view_container_scetion2_list_item_icon'
                                />
                                <Text
                                    className='about_view_container_scetion2_list_item_desc'
                                >
                                    旅游同业<br/>
                                    专属服务
                                </Text>
                                <Text
                                    className='about_view_container_scetion2_list_item_value'
                                >
                                    我们专注于服务旅游同业，围绕行业标准及需求打造产品，让客户感同宾至如归
                                </Text>
                            </Box>
                            <Box
                                className='about_view_container_scetion2_list_item'
                            >
                                <Image
                                    src={Group}
                                    className='about_view_container_scetion2_list_item_icon'
                                />
                                <Text
                                    className='about_view_container_scetion2_list_item_desc'
                                >
                                    精于技术<br/>
                                    极简交互
                                </Text>
                                <Text
                                    className='about_view_container_scetion2_list_item_value'
                                >
                                    我们精于技术赋能产品，更重视优化客户体验，简化操作流程，让您轻松使用产品
                                </Text>
                            </Box>
                            <Box
                                className='about_view_container_scetion2_list_item'
                            >
                                <Image
                                    src={Group}
                                    className='about_view_container_scetion2_list_item_icon'
                                />
                                <Text
                                    className='about_view_container_scetion2_list_item_desc'
                                >
                                    开放合作<br/>
                                    多元开发
                                </Text>
                                <Text
                                    className='about_view_container_scetion2_list_item_value'
                                >
                                    我们相信您有自己的梦想，这里将助力您的业务发展，不论是技术开发与商务合作
                                </Text>
                            </Box>
                        </HStack>
                    </Box>
                    <HStack
                        className='about_view_container_scetion'
                        mt='-32px'
                    >
                        <Image
                            src={isLargerThan768 ? Pic : Pic1}
                            className='about_view_container_scetion_img'
                        />
                        {
                            !isLargerThan768 ? (
                                <Image
                                    src={Pic2}
                                    w='306px'
                                    h='202px'
                                    mx='auto'
                                />
                            ) : (
                                <Box
                                    className='about_view_container_scetion_content'
                                >
                                    <Text
                                        className='about_view_container_scetion_content_title'
                                    >
                                        关于多会儿旅行   
                                    </Text>
                                    <Text
                                        className='about_view_container_scetion_content_subtitle'
                                    >
                                        多会儿旅行致力成为旅游同业的一站式采购平台。 
                                    </Text>
                                    <Text
                                        className='about_view_container_scetion_content_desc'
                                    >
                                        旅点科技旗下的旅游品牌，【多会儿旅行】起始于后疫情时代，聚焦自由行市场的趋势。
                                    </Text>
                                    <Text
                                        className='about_view_container_scetion_content_desc'
                                    >
                                        成立的初衷是为赋能旅游同业拥有全面的产品资源，帮助旅游同业扩大客户群体，提供更高效的服务。 
                                    </Text>
                                </Box>
                            )
                        }
                        
                    </HStack>
                </Box>
                {
                    isLargerThan768 ? (
                        <VStack
                            className='about_view_contect'
                        >
                            <Box
                                className='about_view_contect_main'
                            >
                                <HStack
                                    className='about_view_contect_main_header'
                                >
                                    <Text
                                        as='h1'
                                        className='about_view_contect_main_header_title'
                                    >
                                        与我们联系
                                    </Text>
                                    <Image
                                        src={FrameIcon}
                                        className='about_view_contect_main_header_icon'
                                    />
                                </HStack>
                                <HStack
                                    className='about_view_contect_main_desc'
                                >
                                    <Box
                                        className='about_view_contect_main_desc_item'
                                    >
                                        <Text
                                            className='about_view_contect_main_desc_item_title'
                                        >
                                            售后客服
                                        </Text>
                                        <Text
                                            className='about_view_contect_main_desc_item_value'
                                        >
                                            电话：010-123456
                                        </Text>
                                        <Text
                                            className='about_view_contect_main_desc_item_value'
                                        >
                                            邮箱： reservations@tripintl.com
                                        </Text>
                                    </Box>
                                    <Box
                                        className='about_view_contect_main_desc_item'
                                    >
                                        <Text
                                            className='about_view_contect_main_desc_item_title'
                                        >
                                            API分销/技术合作
                                        </Text>
                                        <Text
                                            className='about_view_contect_main_desc_item_value'
                                        >
                                            电话：010-123456
                                        </Text>
                                        <Text
                                            className='about_view_contect_main_desc_item_value'
                                        >
                                            邮箱： reservations@tripintl.com
                                        </Text>
                                    </Box>
                                    <Box
                                        className='about_view_contect_main_desc_item'
                                    >
                                        <Text
                                            className='about_view_contect_main_desc_item_title'
                                        >
                                            供应商合作
                                        </Text>
                                        <Text
                                            className='about_view_contect_main_desc_item_value'
                                        >
                                            电话：010-123456
                                        </Text>
                                        <Text
                                            className='about_view_contect_main_desc_item_value'
                                        >
                                            邮箱： reservations@tripintl.com
                                        </Text>
                                    </Box>
                                    <Box
                                        className='about_view_contect_main_desc_item'
                                    >
                                        <Text
                                            className='about_view_contect_main_desc_item_title'
                                        >
                                            商务合作
                                        </Text>
                                        <Text
                                            className='about_view_contect_main_desc_item_value'
                                        >
                                            电话：010-123456
                                        </Text>
                                        <Text
                                            className='about_view_contect_main_desc_item_value'
                                        >
                                            邮箱： reservations@tripintl.com
                                        </Text>
                                    </Box>
                                    <Box
                                        className='about_view_contect_main_desc_item'
                                    >
                                        <Text
                                            className='about_view_contect_main_desc_item_title'
                                        >
                                            意见与反馈
                                        </Text>
                                        <Text
                                            className='about_view_contect_main_desc_item_value'
                                        >
                                            电话：010-123456
                                        </Text>
                                        <Text
                                            className='about_view_contect_main_desc_item_value'
                                        >
                                            邮箱： reservations@tripintl.com
                                        </Text>
                                    </Box>
                                </HStack>
                            </Box>
                        </VStack>
                    ) : (
                        <Box
                            className='service_view'
                        >
                            <HStack
                                className='service_view_title'
                            >
                                <Text
                                    className='service_view_title_value'
                                >
                                    我们服务对象
                                </Text>
                                <Image
                                    src={FrameIcon}
                                    className='service_view_title_icon'
                                />
                            </HStack>
                            <Box
                                className='service_view_main'
                            >
                                <HStack
                                    className='service_view_item'
                                    onClick={(e:any)=>{
                                        setShowMenu1(!showmenu1);
                                    }}
                                >
                                    <Text
                                        className='service_view_item_value'
                                    >
                                        售后客服
                                    </Text>
                                    <Image
                                        src={showmenu1 ? DownImage : UpImage}
                                        className='service_view_item_value_icon'
                                    />
                                </HStack>
                                {
                                    showmenu1 &&
                                    <Box
                                        className='service_view_item_submenu'
                                    >
                                        <Text
                                            className='service_view_item_submenu_title'
                                        >
                                            电话：010-123456
                                        </Text>
                                        <Text
                                            className='service_view_item_submenu_title'
                                        >
                                            邮箱： reservations@tripintl.com
                                        </Text>
                                    </Box>
                                }
                            </Box>
                            <Box
                                className='service_view_main'
                            >
                                <HStack
                                    className='service_view_item'
                                    onClick={(e:any)=>{
                                        setShowMenu2(!showmenu2);
                                    }}
                                >
                                    <Text
                                        className='service_view_item_value'
                                    >
                                        API分销/技术合作
                                    </Text>
                                    <Image
                                        src={showmenu2 ? DownImage : UpImage}
                                        className='service_view_item_value_icon'
                                    />
                                </HStack>
                                {
                                    showmenu2 &&
                                    <Box
                                        className='service_view_item_submenu'
                                    >
                                        <Text
                                            className='service_view_item_submenu_title'
                                        >
                                            电话：010-123456
                                        </Text>
                                        <Text
                                            className='service_view_item_submenu_title'
                                        >
                                            邮箱： reservations@tripintl.com
                                        </Text>
                                    </Box>
                                }
                            </Box>
                            <Box
                                className='service_view_main'
                            >
                                <HStack
                                    className='service_view_item'
                                    onClick={(e:any)=>{
                                        setShowMenu3(!showmenu3);
                                    }}
                                >
                                    <Text
                                        className='service_view_item_value'
                                    >
                                        供应商合作
                                    </Text>
                                    <Image
                                        src={showmenu3 ? DownImage : UpImage}
                                        className='service_view_item_value_icon'
                                    />
                                </HStack>
                                {
                                    showmenu3 &&
                                    <Box
                                        className='service_view_item_submenu'
                                    >
                                        <Text
                                            className='service_view_item_submenu_title'
                                        >
                                            电话：010-123456
                                        </Text>
                                        <Text
                                            className='service_view_item_submenu_title'
                                        >
                                            邮箱： reservations@tripintl.com
                                        </Text>
                                    </Box>
                                }
                            </Box>
                            <Box
                                className='service_view_main'
                            >
                                <HStack
                                    className='service_view_item'
                                    onClick={(e:any)=>{
                                        setShowMenu4(!showmenu4);
                                    }}
                                >
                                    <Text
                                        className='service_view_item_value'
                                    >
                                        商务合作
                                    </Text>
                                    <Image
                                        src={showmenu4 ? DownImage : UpImage}
                                        className='service_view_item_value_icon'
                                    />
                                </HStack>
                                {
                                    showmenu4 &&
                                    <Box
                                        className='service_view_item_submenu'
                                    >
                                        <Text
                                            className='service_view_item_submenu_title'
                                        >
                                            电话：010-123456
                                        </Text>
                                        <Text
                                            className='service_view_item_submenu_title'
                                        >
                                            邮箱： reservations@tripintl.com
                                        </Text>
                                    </Box>
                                }
                            </Box>
                            <Box
                                className='service_view_main'
                            >
                                <HStack
                                    className='service_view_item'
                                    onClick={(e:any)=>{
                                        setShowMenu5(!showmenu5);
                                    }}
                                >
                                    <Text
                                        className='service_view_item_value'
                                    >
                                        意见与反馈
                                    </Text>
                                    <Image
                                        src={showmenu5 ? DownImage : UpImage}
                                        className='service_view_item_value_icon'
                                    />
                                </HStack>
                                {
                                    showmenu5 &&
                                    <Box
                                        className='service_view_item_submenu'
                                    >
                                        <Text
                                            className='service_view_item_submenu_title'
                                        >
                                            电话：010-123456
                                        </Text>
                                        <Text
                                            className='service_view_item_submenu_title'
                                        >
                                            邮箱： reservations@tripintl.com
                                        </Text>
                                    </Box>
                                }
                            </Box>
                        </Box>
                    )
                }
            </Box>
        </PageContainer>
    )
}