import {
    HStack,
    Box,
    Image,
    Text,
    Button
} from '@chakra-ui/react';
import {
  useNavigate,
} from 'react-router-dom';
import { useRecoilValue } from 'recoil';
import mediaQueryStateAtom from '~/recoil/atom/mediaQueryState';
import PageContainer from '~/containers/PageContainer';
import ContentContainer from '~/containers/Content';
import Banner from '~/assets/images/index/banner.png';
import BannerImg from '~/assets/images/banner.jpeg';
import logo from '~/assets/images/logo1.jpeg';
import Frame from '~/assets/images/index/Frame.png';
import title from '~/assets/images/index/title.png';
import title1 from '~/assets/images/index/title1.png';
import title2 from '~/assets/images/index/title2.png';
import Line from '~/assets/images/product/Line.png';
import SmallLine from '~/assets/images/smallLine.png';
import icon11 from '~/assets/images/index/icon11.png';
import icon1 from '~/assets/images/product/icon1.png';
import icon3 from '~/assets/images/index/icon3.png';
import icon4 from '~/assets/images/index/icon4.png';
import icon5 from '~/assets/images/index/icon5.png';
import icon6 from '~/assets/images/index/icon6.png';
import icon7 from '~/assets/images/index/icon7.png';
import icon8 from '~/assets/images/index/icon8.png';
import icon9 from '~/assets/images/index/icon9.png';
import icon10 from '~/assets/images/index/icon10.png';
import mobile_icon1 from '~/assets/images/icon1.png';
import mobile_icon2 from '~/assets/images/icon2.png';
import pic from '~/assets/images/index/pic.png';
import pic1 from '~/assets/images/index/pic1.png';
import pic2 from '~/assets/images/index/pic2.png';
import pic3 from '~/assets/images/index/pic3.png';
import './index.css';

const list:any = [
    {
        id: 0,
        icon: icon3,
        desc: '丰富优质的',
        desc1: '产品资源'
    },
    {
        id: 1,
        icon: icon4,
        desc: '行业级API',
        desc1: '轻松对接'
    },
    {
        id: 2,
        icon: icon5,
        desc: '底价或佣金',
        desc1: '多选模式'
    },
    {
        id: 3,
        icon: icon6,
        desc: '便捷的支付',
        desc1: '额度支付'
    },
    {
        id: 4,
        icon: icon7,
        desc: '全方位技术',
        desc1: '专业支持'
    },
    {
        id: 5,
        icon: icon8,
        desc: '个性化需求',
        desc1: '定制服务'
    },
    {
        id: 6,
        icon: icon9,
        desc: '多元的服务',
        desc1: '开放合作',
    },
    {
        id: 7,
        icon: icon10,
        desc: '7*24小时',
        desc1: '专属客服',
    }
]
export default function Home () {
    const { isLargerThan768 } = useRecoilValue(mediaQueryStateAtom);
    const navigate = useNavigate();
    const register = (event: any) => {
        navigate('/register');
    }
    const renderList = () => {
        return (
            <Box
                className='home_view_list'
            >
                <HStack
                    className='home_view_list_main'
                >
                    <Box
                        className='home_view_list_main_item'
                    >
                        <HStack
                            className='home_view_list_main_item_price'
                        >
                            <Text
                                className='home_view_list_main_item_price_value'
                            >
                                120
                            </Text>
                            <Text
                                className='home_view_list_main_item_price_unit'
                            >
                                万
                            </Text>
                        </HStack>
                        <Text className='home_view_list_main_item_icon1'>+</Text>
                        <Text className='home_view_list_main_item_icon2'>+</Text>
                        <Text
                            className='home_view_list_main_item_desc'
                        >
                            全球酒店资源
                        </Text>
                    </Box>
                    <Box
                        className='home_view_list_main_item'
                    >
                        <HStack
                            className='home_view_list_main_item_price'
                        >
                            <Text
                                className='home_view_list_main_item_price_value'
                            >
                                10
                            </Text>
                            <Text
                                className='home_view_list_main_item_price_unit'
                            >
                                万
                            </Text>
                        </HStack>
                        <Text className='home_view_list_main_item_icon1'>+</Text>
                        <Text className='home_view_list_main_item_icon2'>+</Text>
                        <Text
                            className='home_view_list_main_item_desc'
                        >
                            当地玩乐资源
                        </Text>
                    </Box>
                    <Box
                        className='home_view_list_main_item'
                    >
                        <HStack
                            className='home_view_list_main_item_price'
                        >
                            <Text
                                className='home_view_list_main_item_price_value'
                            >
                                2500
                            </Text>
                        </HStack>
                        <Text className='home_view_list_main_item_icon1'>+</Text>
                        <Text className='home_view_list_main_item_icon2'>+</Text>
                        <Text
                            className='home_view_list_main_item_desc'
                        >
                            产品覆盖城市
                        </Text>
                    </Box>
                    <Box
                        className='home_view_list_main_item'
                    >
                        <HStack
                            className='home_view_list_main_item_price'
                        >
                            <Text
                                className='home_view_list_main_item_price_value'
                            >
                                1.7
                            </Text>
                            <Text
                                className='home_view_list_main_item_price_unit'
                            >
                                亿
                            </Text>
                        </HStack>
                        <Text className='home_view_list_main_item_icon1'>+</Text>
                        <Text className='home_view_list_main_item_icon2'>+</Text>
                        <Text
                            className='home_view_list_main_item_desc'
                        >
                            实时预订产品
                        </Text>
                    </Box>
                    <Box
                        className='home_view_list_main_item'
                    >
                        <HStack
                            className='home_view_list_main_item_price'
                        >
                            <Text
                                className='home_view_list_main_item_price_value'
                            >
                                120
                            </Text>
                            <Text
                                className='home_view_list_main_item_price_unit'
                            >
                                万
                            </Text>
                        </HStack>
                        <Text className='home_view_list_main_item_icon1'>+</Text>
                        <Text className='home_view_list_main_item_icon2'>+</Text>
                        <Text
                            className='home_view_list_main_item_desc'
                        >
                            全球商户选择
                        </Text>
                    </Box>
                </HStack>
            </Box>
        )
    }
    return (
        <PageContainer>
            <Box
                className='home_view'
            >
                
                {
                    isLargerThan768 ? (
                        <Box
                            className='home_banner'
                        >
                            <Image
                                src={logo}
                                className='home_banner_logo'
                            />
                            <Image
                                src={title}
                                className='home_banner_title'
                            />
                            <Image
                                src={Banner}
                                className='home_banner_container'
                            />
                            <Text
                                className='home_banner_desc'
                            >
                                轻松获取优质资源， 多种合作方式
                            </Text>
                            <Button
                                className='home_banner_action'
                                onClick={(e)=>register(e)}
                            >
                                免费注册
                            </Button>
                        </Box>
                    ) : (
                        <Image
                            src={BannerImg}
                        />
                    )
                }
                {
                    isLargerThan768 && renderList()
                }
            </Box>
            <ContentContainer
                h='186px'
                minW={isLargerThan768? '1512px' : '100%'}
                bg='#2D6CDF'
            >
                <Box
                    className='home_view_section'
                >
                    <Text
                        className='home_view_section_title'
                    >
                        多会儿旅行与旅业相融共生
                    </Text>
                    <Image
                        src={isLargerThan768 ? Line : SmallLine}
                        className='home_view_section_line'
                    />
                    <Text
                        className='home_view_section_desc'
                    >
                        从旅业需求出发，围绕客户的经营场景，解决用户需求
                    </Text>
                </Box>
            </ContentContainer>
            <Box
                className='home_view_desc'
            >
                {
                    isLargerThan768 ? (
                        <Text
                            className='home_view_desc_value'
                        >
                            多会儿旅行融合现代互联网技术，通过自建系统及分销网站，以数字化酒店、当地玩乐产品为代表，打造了全球核心旅游资源整合与分销的业务体系，成为领先的旅游资源分销平台与技术服务商。 <br/>
                            打造了全球核心旅游资源整合与分销的业务体系，成为领先的旅游资源分销平台与技术服务商。<br/>
                            多会儿旅行提供了全球120万家酒店资源，10万个当地玩乐产品，对接了全球千家大型批发商资源及万家知名酒店集团直签资源，<br/>
                            合作伙伴数量超过万家，涵盖了多类线下及线上客户，包活旅行社、商旅公司、会展公司、定制公司、同业分销平台、小众旅游平台等。<br/>
                        </Text>
                    ) : (
                        <Text
                            className='home_view_desc_value'
                        >
                            多会儿旅行融合现代互联网技术，通过自建系统及分销网站，以数字化酒店、当地玩乐产品为代表，打造了全球核心旅游资源整合与分销的业务体系，成为领先的旅游资源分销平台与技术服务商。 <br/>
                            多会儿旅行提供了全球120万家酒店资源，10万个当地玩乐产品，对接了全球千家大型批发商资源及万家知名酒店集团直签资源，合作伙伴数量超过万家，涵盖了多类线下及线上客户，包活旅行社、商旅公司、会展公司、定制公司、同业分销平台、小众旅游平台等。
                        </Text>
                    )
                }
                {
                    !isLargerThan768 &&
                    <Image
                        src={mobile_icon1}
                        className='home_view_desc_icon2'
                    />
                }
                {
                    !isLargerThan768 &&
                    <Image
                        src={mobile_icon2}
                        className='home_view_desc_icon1'
                    />
                }
            </Box>
            <Box
                className='home_view_main'
            >
                <HStack
                    className='home_view_main_bussiness'
                >
                    <Box
                        className='home_view_main_bussiness_item'
                    >
                        <Image
                            src={icon11}
                            className='home_view_main_bussiness_item_tip'
                        />     
                        <Image
                            src={icon1}
                            className='home_view_main_bussiness_item_icon'
                        />
                        <Text
                            className='home_view_main_bussiness_item_title'
                        >
                            酒店
                        </Text>
                        <Text
                            className='home_view_main_bussiness_item_subtitle'
                        >
                            超120万全球酒店资源
                        </Text>
                        <Text
                            className='home_view_main_bussiness_item_desc'
                        >
                            酒店覆盖6大洲，超200个国家地区！<br/>
                            实时预订库存超60万价格优惠，预定无忧
                        </Text>
                        <Button
                            className='home_view_main_bussiness_item_action'
                        >
                            了解更多
                        </Button>
                    </Box>
                    <Box
                        className='home_view_main_bussiness_item'
                    >
                        <Image
                            src={icon11}
                            className='home_view_main_bussiness_item_tip'
                        />     
                        <Image
                            src={icon1}
                            className='home_view_main_bussiness_item_icon'
                        />
                        <Text
                            className='home_view_main_bussiness_item_title'
                        >
                            当地玩乐
                        </Text>
                        <Text
                            className='home_view_main_bussiness_item_subtitle'
                        >
                            超10万活动门票等玩乐产品
                        </Text>
                        <Text
                            className='home_view_main_bussiness_item_desc'
                        >
                            资源覆盖全球600个城市地区，产品包括当季热门活动、景区、交通
                            等，规划完美行程，出游无负担！
                        </Text>
                        <Button
                            className='home_view_main_bussiness_item_action'
                        >
                            了解更多
                        </Button>
                    </Box>
                </HStack>
            </Box>
            {
                !isLargerThan768 && renderList()
            }
            <Box
                className='home_view_content'
            >
                <Image
                    className='home_view_content_title'
                    src={title1}
                />
                <HStack
                    className='home_view_content_section'
                >
                    <Image
                        src={pic1}
                        className='home_view_content_section_icon'
                    />
                    <Box
                        className='home_view_content_section_main'
                    >
                        <HStack
                            className='home_view_content_section_main_title'
                        >
                            <Text
                                className='home_view_content_section_main_title_value'
                            >
                                使用预定系统
                            </Text>
                            <Image
                                src={Frame}
                                className='home_view_content_section_main_title_icon'
                            />
                        </HStack>
                        <Text
                            className='home_view_content_section_main_subtitle'
                        >
                            多终端
                        </Text>
                        <Text
                            className='home_view_content_section_main_desc'
                        >
                            覆盖电脑端、手机端、小程序的预定系统
                        </Text>
                        <Text
                            className='home_view_content_section_main_subtitle'
                        >
                            快捷支付
                        </Text>
                        <Text
                            className='home_view_content_section_main_desc'
                        >   
                            支持各类支付模式， 支付宝，微信，信用卡，额度支付
                        </Text>
                        <Text
                            className='home_view_content_section_main_subtitle'
                        >
                            7*24小时客服
                        </Text>
                        <Text
                            className='home_view_content_section_main_desc'
                        >
                            中英文客服，全天候在线，随时可为您引导护航
                        </Text>
                        <Button
                            className='home_view_content_section_main_action'
                        >
                            立即注册
                        </Button>
                    </Box>
                </HStack>
                <HStack
                    className='home_view_content_section'
                >
                    <Box
                        className='home_view_content_section_mains'
                    >
                        <HStack
                            className='home_view_content_section_main_title'
                        >
                            <Text
                                className='home_view_content_section_main_title_value'
                            >
                                API分销 
                            </Text>
                            <Image
                                src={Frame}
                                className='home_view_content_section_main_title_icon'
                            />
                        </HStack>
                        <Text
                            className='home_view_content_section_main_subtitle'
                        >
                            标准API
                        </Text>
                        <Text
                            className='home_view_content_section_main_desc'
                        >
                            行业级别的API接口，轻松人性化，中文文档
                        </Text>
                        <Text
                            className='home_view_content_section_main_subtitle'
                        >
                            专业技术支持
                        </Text>
                        <Text
                            className='home_view_content_section_main_desc'
                        >
                            超过7+年的API对接经营的技术服务团队，提供专属的一对一对接支持
                        </Text>
                        <Text
                            className='home_view_content_section_main_subtitle'
                        >
                            完整产品输出
                        </Text>
                        <Text
                            className='home_view_content_section_main_desc'
                        >
                            获得最全量的产品，包含保真度高的静态数据
                        </Text>
                        <Button
                            className='home_view_content_section_main_action'
                        >
                            申请对接
                        </Button>
                    </Box>
                    <Image
                        src={pic2}
                        className='home_view_content_section_icons'
                    />
                </HStack>
                <HStack
                    className='home_view_content_section'
                >
                    <Image
                        src={pic3}
                        className='home_view_content_section_icon'
                    />
                    <Box
                        className='home_view_content_section_main'
                    >
                        <HStack
                            className='home_view_content_section_main_title'
                        >
                            <Text
                                className='home_view_content_section_main_title_value'
                            >
                                客制化服务
                            </Text>
                            <Image
                                src={Frame}
                                className='home_view_content_section_main_title_icon'
                            />
                        </HStack>
                        <Text
                            className='home_view_content_section_main_subtitle'
                        >
                            接口开发服务
                        </Text>
                        <Text
                            className='home_view_content_section_main_desc'
                        >
                            提供接口开发服务，为您节省时间及开发成本
                        </Text>
                        <Text
                            className='home_view_content_section_main_subtitle'
                        >
                            B2B2C网站服务
                        </Text>
                        <Text
                            className='home_view_content_section_main_desc'
                        >
                            轻松搭建自己的线上站点，拥有自己的OTA
                        </Text>
                        <Text
                            className='home_view_content_section_main_subtitle'
                        >
                            其他定制化需求开发
                        </Text>
                        <Text
                            className='home_view_content_section_main_desc'
                        >
                            本地化服务部署，专属DNS配置/独立域名部署
                        </Text>
                        <Button
                            className='home_view_content_section_main_action'
                        >
                            联系我们
                        </Button>
                    </Box>
                </HStack>
            </Box>
            <Box
                className='home_view_contents'
            >
                <Image
                    className='home_view_content_title'
                    src={title2}
                />
                <HStack
                    className='home_view_content_list'
                >
                    {
                        list.map((item:any)=>
                            <HStack
                                className='home_view_content_list_item'
                            >
                                <Image
                                    src={item.icon}
                                    className='home_view_content_list_item_icon'
                                />
                                <Box>
                                    <Text
                                        className='home_view_content_list_item_desc'
                                    >
                                        {item.desc}
                                    </Text>
                                    <Text
                                        className='home_view_content_list_item_desc'
                                    >
                                        {item.desc1}
                                    </Text>
                                </Box>
                            </HStack>
                        )
                    }
                </HStack>
            </Box>
            <ContentContainer
                h={isLargerThan768 ? '420px' : '565px'}
                bg='#2D6CDF'
            >
                <HStack
                    className='home_view_gift'
                >
                    <Image
                        src={pic}
                        className='home_view_gift_icon'
                    />
                    <Box
                        className='home_view_gift_content'
                    >
                        <Text
                            className='home_view_gift_content_title'
                        >
                            现在注册<br/>获得新客优惠大礼包
                        </Text>
                        <Image
                            src={isLargerThan768 ? Line : SmallLine}
                            className='home_view_gift_content_line'
                        />
                        <Text
                            className='home_view_gift_content_desc'
                        >
                            不玩虚的，实打实的优惠
                        </Text>
                        <Button
                            className='home_view_gift_content_action'
                            onClick={(e)=>register(e)}
                        >
                            免费注册
                        </Button>
                    </Box>
                </HStack>
            </ContentContainer>
        </PageContainer>
    )
}