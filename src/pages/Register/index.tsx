import {
    Flex,
    Checkbox,
    Box,
    Text,
    Input,
    FormLabel,
    FormControl,
    Switch,
    Button,
    Image,
    Center
} from '@chakra-ui/react';
import AuthRegister from '~/awsHttpApi/register';
import {
  useNavigate
} from 'react-router-dom';
import PageContainer from '~/containers/PageContainer';
import UserIcon from '~/assets/images/user.png';
import HomeIcon from '~/assets/images/home.png';
import PhoneIcon from '~/assets/images/phone.png';
import './index.css';
import { useState } from 'react';

export default function Home () {
    const navigate = useNavigate();
    const [companyName, setCompanyName] = useState<string>('');
    const [contactName, setContactName] = useState<string>('');
    const [contactPhone, setContactPhone] = useState<string>('');
    const [api, setApi] = useState<string>('ON');
    /**
     * 查看协议
     */
    const showAgree = (event:any) => {
        event.nativeEvent.stopImmediatePropagation();
        // TermsConditions
        navigate('/TermsConditions');
    }
    const doRegister = (event: any) => {
        let parmas = {
            type: 'enterprise',
            companyName: companyName,
            contactName: contactName,
            contactPhone: contactPhone,
            api: api,
        }
        AuthRegister(parmas)
            .then(res=>{
                console.log('res', res)
            })
    }
    const handleChange = (type: any, event: any) => {
        let value = event.target.value;
        if (type === 'contactName') {
            setContactName(value);
        }
        if (type === 'companyName') {
            setCompanyName(value);
        }
        if (type === 'contactPhone') {
            setContactPhone(value);
        }
    }
    const Login = (event:any) => {
        window.open('http://b2b.tripintl.com/login', '_blank');
    }
    return (
        <PageContainer>
            <Box
                className='register_view'
            >
                <Box
                    className='register_view_main'
                >
                    <Box className='register_view_main_container'>
                        <Text
                            className='register_view_main_title'
                        >
                            立即注册
                        </Text>
                        <Box
                            className='register_view_main_control'
                        >
                            <Image
                                src={UserIcon}
                                className='register_view_main_control_icon'
                            />
                            <Input
                                type='text'
                                className='register_view_main_control_input'
                                placeholder='请输入你的姓名'
                                value={contactName}
                                onChange={(e) => handleChange('contactName', e)}
                            />
                        </Box>
                        <Box
                            className='register_view_main_control'
                        >
                            <Image
                                src={HomeIcon}
                                className='register_view_main_control_icon'
                            />
                            <Input
                                type='text'
                                className='register_view_main_control_input'
                                placeholder='请输入企业公司的名称'
                                value={companyName}
                                onChange={(e) => handleChange('companyName', e)}
                            />
                        </Box>
                        <Box
                            className='register_view_main_control'
                        >   
                            <Image
                                src={PhoneIcon}
                                className='register_view_main_control_icon'
                            />
                            <Input
                                type='text'
                                className='register_view_main_control_input'
                                placeholder='请输入您常用的手机号码'
                                value={contactPhone}
                                onChange={(e) => handleChange('contactPhone', e)}
                            />
                        </Box>
                        <Flex
                            className='register_view_main_info'
                        >
                            <Text></Text>
                        </Flex>
                        <FormControl
                            className='register_view_main_info'
                        >
                            <FormLabel
                                htmlFor='apply-user'
                                className='register_view_main_info_label'
                            >
                                申请多会儿旅行用户
                            </FormLabel>
                            <Switch
                                id='apply-user'
                            />
                        </FormControl>
                        <FormControl
                            className='register_view_main_info'
                        >
                            <FormLabel
                                htmlFor='api-butt'
                                className='register_view_main_info_label'
                            >
                                申请API接口对接
                            </FormLabel>
                            <Switch
                                id='api-butt'
                                isChecked={api === 'ON' ? true : false}
                                onChange={(e:any)=>{
                                    let checked:boolean = e.target.checked;
                                    if (checked) {
                                        setApi('ON')
                                    } else {
                                        setApi('OFF')
                                    }
                                }}
                            />
                        </FormControl>
                        <Checkbox
                            defaultChecked
                            className='register_view_main_agree'
                        >
                            <Flex>
                                <Text
                                    className='register_view_main_agree_label'
                                >
                                    我已阅读并同意
                                </Text>
                                <Text
                                    className='register_view_main_agree_value'
                                    onClick={(e)=>showAgree(e)}
                                >
                                    《多会儿旅行采购服务条款》
                                </Text>
                            </Flex>
                        </Checkbox>
                        <Center>
                            <Button
                                className='register_view_main_submit'
                                onClick={(e)=>doRegister(e)}
                            >
                                免费注册
                            </Button>
                        </Center>
                        <Flex
                            className='register_view_main_tip'
                        >
                            <Text
                                className='register_view_main_tip_label'
                            >
                                已经注册过了嘛？
                            </Text>
                            <Text
                                className='register_view_main_tip_login'
                                onClick={(e)=>Login(e)}
                            >
                                马上登入
                            </Text>
                        </Flex>
                    </Box>
                </Box>
            </Box>
        </PageContainer>
    )
}