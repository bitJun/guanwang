import { Flex, Box, FlexProps, useDisclosure, BoxProps } from '@chakra-ui/react';

import Content from './Content';
import Header from '~/components/Header';
import Footer from '~/components/Footer';
import NavBarModal from '~/components/NavBarModal';
import RegisterTip from '~/components/RegisterTip';
import SuccessTip from '~/components/SuccessTip';
import { useRecoilState } from 'recoil';
import modalControlAtom from '~/recoil/atom/modalControl';

type PageContainerProps = {
  children?: React.ReactNode;
  containerStyles?: FlexProps;
  topBoxStyle?: BoxProps;
  hideHeader?: boolean;
  hideFooter?: boolean;
  afterHeaderComponent?: React.ReactNode;
};

export default function PageContainer({
  children,
  containerStyles = {},
  topBoxStyle = {},
  hideFooter = false,
  hideHeader = false,
  afterHeaderComponent = null,
}: PageContainerProps) {
  const { onOpen } = useDisclosure();
  const [
    {
      isNavModalOpen,
      isRegisterTipModalOpen,
      isSuccessTipModalOpen
    },
    setModalControl,
  ] = useRecoilState(modalControlAtom);
  const handleNavBarModalClose = () => {
    setModalControl((prev) => ({ ...prev, isNavModalOpen: false }));
  }
  const handleRegisterTipModalClose = () => {
    setModalControl((prev) => ({ ...prev, isRegisterTipModalOpen: false }));
  }
  const handleSuccessTipModalClose = () => {
    setModalControl((prev) => ({ ...prev, isSuccessTipModalOpen: false }));
  }
  return (
    <Flex direction="column" align="stretch" bg="#FAFAFA" minH="100vh" {...containerStyles}>
      <Box pos="sticky" top={0} zIndex={100} {...topBoxStyle}>
        {!hideHeader && (
          <Box as="header" bg="white" borderBottomWidth="1px">
            <Content m="auto">
              <Header onOpen={onOpen} />
            </Content>
          </Box>
        )}
        {afterHeaderComponent}
      </Box>
      <Box
        as="main"
        flex="1"
        bgColor="white"
        w='100%'
        mx='auto !important'
      >
        {children}
      </Box>
      {!hideFooter && (
        <Content as="footer" m="auto">
          <Footer />
        </Content>
      )}
      <NavBarModal isOpen={isNavModalOpen} onClose={handleNavBarModalClose} />
      <RegisterTip isOpen={isRegisterTipModalOpen} onClose={handleRegisterTipModalClose} />
      <SuccessTip isOpen={isSuccessTipModalOpen} onClose={handleSuccessTipModalClose} />
    </Flex>
  );
}
