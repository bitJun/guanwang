# tzuchi customer front-end

### page component progress
- [X] Home
- [X] PickMealLocationInfo
- [X] AddItem
- [X] EditItem
- [X] StoreList
- [X] Store
  - [X] StoreInfo
- [X] MemberCenter
- [X] OrderList
- [X] MealBoxRecycling
  - [X] LocationInfo
  - [X] ReturnList
  - [X] ReturnRecord
- [X] Cart
- [X] PickOrderTimeModal
- [X] PickMealLocationModal
### routing
- Home
  - FilterMeal
- PickMealLocationInfo
- AddItem
- EditItem
- StoreList
- Store
  - StoreInfo
- MemberCenter
- OrderList
- MealBoxRecycling
  - LocationInfo
  - ReturnList
  - ReturnRecord
- Cart
### theme setting
|     name     | color code |
| :----------: | ---------- |
|   primary    | #E6B369    |
| primaryLight | #E8D3B4    |
|  secondary   | #674E29    |
|  fontColor   | #333333    |
|     dark     | #333333    |
|   gray.100   | #F8F8F8    |
|   gray.200   | #F2F2F2    |
|   gray.300   | #D9D9D9    |
|   gray.400   | #C4C4C4    |
|   gray.500   | #8D8D8D    |
|   gray.700   | #454545    |
|  danger.200  | #FFF4F4    |
|  danger.300  | #FCB4B4    |
|  danger.600  | #E83B3B    |

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
